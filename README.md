# Zabbix Server 4

```bash
docker run --net=host \
            -v /mnt/tech/zabbix/conf:/etc/zabbix \
            -v /mnt/tech/zabbix/log:/var/log/zabbix \
            -v /mnt/tech/web/conf:/usr/share/webapps/zabbix/conf \
            -v /mnt/tech/web/log:/var/log/lighttpd \
            <image_name>
```
