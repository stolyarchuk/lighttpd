FROM alpine:3.15
LABEL maintaner="Roman Stolyarchuk <roman@stolyarch.uk>"

ENV TZ=Europe/Moscow

RUN apk add --update --no-cache \
    tzdata lighttpd php7-common php7-iconv php7-json php7-gd php7-bcmath php7-xmlreader php7-mbstring \
    php7-curl php7-xml php7-pgsql php7-mysqli php7-imap php7-cgi php7-session fcgi php7-pdo php7-pdo_mysql php7-sockets \
    php7-soap php7-xmlrpc php7-posix php7-mcrypt php7-gettext php7-ldap php7-ctype php7-dom \
    bash curl && \
    rm -rf /var/www/localhost/htdocs && \
    curl -sSL https://downloads.ioncube.com/loader_downloads/ioncube_loaders_lin_x86-64.tar.gz -o ioncube.tar.gz && \
    tar -xf ioncube.tar.gz && \
    mv ioncube/ioncube_loader_lin_7.4.so /usr/lib/php7/modules/ && \
    echo 'zend_extension = /usr/lib/php7/modules/ioncube_loader_lin_7.4.so' >  /etc/php7/conf.d/00_ioncube.ini && \
    rm -rf ioncube && \
    ln -sf /usr/bin/php-cgi7 /usr/bin/php-cgi && \
    mkdir -p /run/lighttpd && chown lighttpd /run/lighttpd && \
    rm -rf /var/cache/apk/*

COPY etc/ /etc/

CMD ["/usr/sbin/lighttpd", "-D", "-f", "/etc/lighttpd/lighttpd.conf"]
